<!--
 * @Author: hua
 * @Date: 2020-02-07 19:02:54
 * @description: 
 * @LastEditors: hua
 * @LastEditTime: 2020-04-02 17:43:06
 -->
```
principal:adj. 主要的；资本的，n. 首长；校长；资本；当事人
childhood:幼年
recent:近代的
infancy:初期
opportunity:时机
material：用具
toys:玩具 
encourage:vt 鼓励，支持
assist：v. 参加，出席；协助（做一部分工作）；（通过提供金钱或信息）帮助；在场（当助手）；使便利；（在做某任务中）有助益;n. 帮助；（体育比赛中的）助攻；辅助机械专置
therefor:因此
stages:计划
infant:n. 婴儿，幼儿;未成年人;初学者，生手;adj. 婴儿的，幼儿的;幼稚的，幼小的;初期的;未成年的;
suitable：合适
standard:标准
determined:决定
within:内部
inherited:继承
profit:有益于
underestimate:低谷
stimulate:vt. 刺激;激励，鼓舞;使兴奋;
curiosity:好奇心
bound:界限
available:可获得的
experiment:实验
reach:到达
profit:有益
discover：揭开，揭露
particular:特定
jigsaw:n. 竖锯，钢丝锯;拼板玩具;锯曲线机;
painting:绘画
scribbling：乱涂乱画
sand:n. 沙;沙地；沙洲；沙滩；沙子;沙色;
imaginative:adj. 想象力
pretend：vt. 假装，伪装;假称;装扮;
vi. 扮演;自称;假装，矫作;
adj. 仿制的; 
master:掌握，主
significance:n. 意义;意思;重要性;
valuable:有价值的
relaxation:n. 消遣，放松;松懈，松弛;放宽;[生理]驰松;
matures:成熟
inspire:启发
accord：根据
nowadays:如今
measuring:测量
foretell:预示
political:adj. 政治的；党派的
influence:影响
solely:adv. 单独地；仅仅
remain:保持
strength:力量
measure:测量
military:n. 军队；军人
adj. 军事的；军人的；适于战争的
effectiveness:n. 效力
tried to:试图
competitiveness:竞争力
Soviet Union:前苏联
third-world nation:第三国家
adapt：适应
evolving:v. （使）逐渐发展；进化；释放气体（或热量）（evolve 的现在分词）
emphasis:重点
compete:竞争
complete:完成
industrial:工业
turn on:朝哪
shaping:塑造
direct:adj. 直接的；直系的；亲身的；恰好的
vt. 管理；指挥；导演；指向
adv. 直接地；正好；按直系关系
recognize:认识
course of：课程化
technology:科技局
purpose:目的
threat:威胁
effort:努力
defense:防御
upon:在...之上
contribution:贡献
indicate：表示
purpose:目的
demonstrate:演示
emphasize:注重
prowess:n. 英勇；超凡技术；勇猛
declining:下降
extra:额外
drug:毒品
alcohol:酒精
crazy:adj. 疯狂的；狂热的，着迷的
circle:圈子
pron. 谁（who的宾格）
occupies:占用
central：中央
workaholics：工作狂
stereotype:刻板，对……存有成见
praise:称赞
criticized:批评
earning:n. 收入；所赚的钱
promotion:n. 提升，[劳经] 晋升；推销，促销；促进；发扬，振兴
view:查
neglect:忽略
aspect:n. 方面；方向；形势；外貌
leisure:休闲
equilibrium:n. 均衡；平静；保持平衡的能力
regard:尊敬
unusual:不常见
pleasure:快乐
prefer:更喜欢
since:因此
occupied :v. 占有（occupy的过去分词）
divorce:分离
addicted:v. 使……上瘾；沉迷于……（addict 的过去式和过去分词）
adj. 沉溺于某种（尤其是不良的）嗜好的；入了迷的，上了瘾的
```
achieve:取得
psychologically:adv. 心理上地；心理学地
distanced:v. （使）远离，疏远；领先于（一匹马）（distance 的过去式及过去分词
focus:聚焦
appreciate：感激
attention：注意力
rid of:接触
promotion:促进，晋升
common:  n. 普通；平民；公有地
adj. 共同的；普通的；一般的；通常的
worst:最坏部分
land:大地
extremes:极大地
gap:差距
stand:立场
sadly：令人遗憾
wide:宽
income:收入
inequality：不平均的
yet:然而
burden：义务
benefit:益处
led to：导致
hugely:adv. 极度；巨大地，深深地
theory:学说
household:一家人
wealth:钱财
cross:n. 交叉，十字；十字架，十字形物
adj. 交叉的，相反的；乖戾的；生气的
vt. 杂交；渡过；使相交
reality:n. 现实；实际；真实
tax:税收
bite：微
French fries:炸薯条
pledging:保证，承诺
expert:专家
cue:提示
instance:实例
chain:链
kid:儿童
automatically:自动的
slice:切片
employee:雇员
president:总统
assemble:聚集
campaign:v. 领导或参加运动；参加竞选
expect:预计
announce：vt. 宣布；述说；预示；播报
participating:v. 参加；参与（participate 的现在分词）
dairy:牛奶
side：n. 方面；侧面；旁边
criteria:原则
concern:担心
available:可用
possible：可能
recommend:推荐
specify:vt. 指定；详细说明；列举；把…列入说明书
nationwide:全国性的
species:物种
variety:不同种类
```

