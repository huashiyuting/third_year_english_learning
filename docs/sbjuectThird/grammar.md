<!--
 * @Author: hua
 * @Date: 2020-09-17 10:17:36
 * @description: 
 * @LastEditors: hua
 * @LastEditTime: 2020-09-30 17:13:39
-->
### 语法

#### 句子是人类语言的核心构造，动词是句子的核心。所以学习英语语法需要从句子（sentence）开始。
学习语法->分析句子->分析句子成分->理清句子关系

### 句子成分
1.主语：句子的陈述的对象。
2.谓语：主语发出的动作。一般是有动作意义的动词。
3.宾语：分为动词宾语和介词宾语，后者是动词的承受者。
4.系动词：表示状态或状态变化的动词，没有实际的动作意义。比如be动词（am,is,are）;感官动词（look,sound,smell,taste,feel）;保持类系统词（keep,stay,remain）;状态变化类系动词（become,get,turn,go）等。
5.表语：紧跟系动词后面的成分。
6.定语：修饰名词或代词的成分。
7.状语：修饰形容词，副词，动词或者句子的成分。
8.补语：分为宾语补足语和主语补足语。是对宾语和主语的补充说明 
，与其有主动或被动的逻辑关系。

adv.一般用来修饰动词、形容词、全句或其他副词的词。
### 句型
句子的基本结构：主体部分+谓语部分（名词+动词）

### 五种句型
模式 
    主 系 表 I am a student I主语 am系动词 a student表语
    主 谓 I am studying I主语 am studying谓语
    主 谓 宾 I study English I主语 study谓语 English宾语
    主 谓 间宾 直宾 Our teacher taught us English   Our teacher主语 taught谓语 us间宾 English直宾
    主 谓 宾 宾补 I learn English well I主语 learn谓语 English宾语 well宾补

### 主语+系动词+主语补足语（表语）
系动词（Linking Verb）

### 作用:
无具体动作，仅仅起联结作用
### 后面所接成分：
说明主语特点性质特征
### 种类：
be动词（am,is,are) look,sound,smell,taste,feel,seem,appear,become,turn
### 表语：
名词 or 形容词
### 主语+谓语（+状语）
不及物动词（intransitive Verb）vi. 特点：助于自身可以完成，不需要作用对象 习惯：带状语（修饰动作的成分）
### 主语+谓语+宾语
及物动词（Transitive Verb）bt. 作用：说明主语作动作的作用对象 宾语：主语动作承受对象
### 主语+谓语+间宾+直宾
双宾动词（Dative Verb）特点：后面成分有人（间接宾语[接受者]）又有物（直接宾语[承受者]）
### 主语+谓语+宾语+宾语补足语
宾补动词（Factitive Verb）
### 区分双宾语和复和宾语
在间接宾语后加上be动词，若能成句，则是补足语

### 动词的分类

### 实意动词（Notional Verb）
词义：完整 作用：能独立充当谓语 分类：助动词和情态动词以外的动词
### 助动词（Axiliary Verb）
词义：不完整 作用：无法独立充当谓语必须和实义动词连用，构成各种时态，语态， 语气，否定，疑问
分类：Be:am，is，are,was,were,been,being;
     Do:does,did
     have:has,had,having
### Be类
变化形式：am, is, are, was, were, been, being 功能： ​ 1，帮助构成进行时态 ​ ​ I am studying grammar. ​ ​ He is playing football. ​ 2，帮助构成被动语态 ​ ​ I was cheated.

### Do类
变化形式：Does, did 功能： ​ 1，帮助实义动词构成否定 ​ ​ I do not like English. ​ 2，帮助实义动词构成疑问 ​ ​ Do you like English?

### Have类
变化形式：Has, had, having 功能： ​ 1，帮助构成完成时态 ​ ​ I have studied English for 3 years.

be, do, have可作实义动词又可作助动词

### 情态动词（Modal Verb）
词义：有词义，表示说话者对某种行为或状态的看法或态度，表示可能和建议，愿望，必要，允许，能力，怀疑
作用：无法独立充当谓语，必须和实义动词一起构成复和谓语
常见： can/could/may/might/must/shall/should/will/would/have to/ought to/used to/need/dare

### 句子的变化

### 陈述句否定

### 谓语动词含有助动词或情态动词
变形：助动词或情态动词后+not 例：He is a teacher.He is not a teacher. I can swim. I cannot swim. He will come to the party. He will not come to the party.

### 谓语动词是实义动词
变形：接住助动词do not来构成，第三人称用dose+not+动词原形，过去式did+not,例：I like English. I do not like English. He likes English. He does not like English. There are some dogs. There aren't any dogs.

### 助动词否定的缩写

### 情态动词否定的缩写

其他词变化： and>or; already->yet; both->either; some->any
祈使句：祈使句前+don't 例：Don't open the door.
不定式：不定式前+not 例：She asks the boy not to play in the street.

### 一般疑问句

### 谓语获此含有助动词或情态动词
变形：助动词 or情态动词移至句首 例： He is a teacher.Is he a teacher? Yes,he is/No, he isn't

### 谓语动词是实义动词
变形：加do,does,did于句首，实义动词变原型 例：He likes English,. Does he like English? Yes, he does/No, he doesn't. ​ I like English. Do you like English? Yes, I do/No, I don't.

### 特殊疑问句

### 不接名词连用的疑问句
对人提问：who He can sing in English.Who can sing in English. I saw him at the party last night.Who did you see at the party last night?
对事或物：what i like English. What do you like? I am studying English grammar.What are you doing? I am studying English grammar.What are you doing? I am studying English grammar. What are you studying?I'd like to go swimming tomorrow. What would you like to do tomorrow?

### 对时间提问：
when i was born in 1980.when were you born?
### 对地点提问：
where He lives in Beijing.Where dose he live?
### 对方式提问：
how He goes to school by bus. How does he go to school?
### 对原因提问：
Why i often study at the library because it's quiet.Why do you often study at the libaray?

### 接名词连用的疑问句
Which: Could you lend me your pen?Sure.I have two pens.This pen has black ink. That pen has red ink.Which pen/Which one/Which do you want? That red one. Thanks. ​ Which也可不接名词，这时which用作代词。
Whose: 必须接名词 This is his book.Whose book is this?I borrowed Jack's car last night.Whose car did you borrow last night?

### How用法详解
单独使用：对动作方式的提问 how do you go to work? I drive/By car/I take a coffee/By bus. How did he break his leg? He fell of the ladder.
和形容词or副词连用 How old are you? How tall is he? How big is your new house? How far is it from your home to school? How well dose he speak English? How quickly can you get here?
对频率提问：how often/how many times? I write to my parents once a month.How often do you write to you parents? I go shopping twice a week?.How often do you go shoppong?/How many times a week do you go shopping?/How many times a week do you go shopping?
其他频率短语：（Every/Once a/Twice a/Three times a）（day/week/month/year）

### 时态

### 一般时态和现在时态

### 一般现在时
The present simple tense is used to express a general truth or fact, or an action that occurs regularly or habitually.Generally,the present simple tense verb conveys a sense of permanence. Truth or fact.The sun rise in the east and sets in the west.The earth moves around the sun.An action that occurs rregularly or habitually i often spend two hours reading English in the morining. Classes begin at nine in the morining.
1.事实 The world is round.
2.经常性，习惯性动作或状态 He doesn't work hard.常连用频率副词（助动词后，实义动词前）always frequently usually sometimes generally occasionally often never seldom rarely
3.以there或here开头句子中，表正在发生的短暂动作 Here comes your wife = your wife is coming. There goes our bus;we'll have to wait for the next one.
4.条件状语（if unless）,时间状语（when as soon as before after）从句中，表示将来动作Please let me know when he comes back.What are you going to do when you leave school?I'll be glad if she comes over to visit me.

### 一般过去时
The past simple tense is used to express a completed action which took place eat a specified time in the past.The specified time is either stared or impliied. A completed action i saw him in the library yesterday morning.I began to learn English ten years ago.A past action that occurred regularly or habitually I selpt for eight hours last night.She lived in our town for three years,but now she is living in Beijing.
1.过去动作或状态 He was late for school this morning. I bought this computer three years ago.
2.过去一段时间一直持续或反复发生的动作 I lived in the country for ten years. He used to do morning exercises.He took a walk after supper when he was alive.

### 一般将来时
The future simple tense is used to express an action that will occur at some time in the future. Will or Be Going To can be used to express sort of certainty.According to the weather report, it will be windy tomorrow.(说话人人为将要发生)According to the weather report, it is going to be windy tomorrow.(根据明显迹象判断)Be Going To is usef to express a denfinit plan. I have bought a computer and i'm going to learn the computer science. Will is used to express a willingness.The telephone is ringing; I will answer it. will(说话时做出的决定) be going to(对话前做出的决定)

### 进行时态
时态构成 助动词+进行分词be+doing意义 该时刻（具体时间，另一个具体活动背景下），活动正在进行

### 现在进行时
1.说话此刻正在进行 What program are you watching? He is not available now.He is talking on another phone.
2.现阶段正在持续的动作 what are you doing these days? I am learning the usage of verb tenses.
3.最近的将来已定的安排（计划 安排做）What are you doing on Saturday night? I'm doing some shopping with Jane.I am taking a makeup test tomorrow.
4.与always forever continually constantly等连用，表示抱怨，厌烦 Jack is always borrowing money and forgetting to pay you back. He is continually asking me for money.

### 过去进行时
1.过去特定时刻发生的事情 I was discussing my thesis with my director at this time last night.What were you doing at 10 o'clock last night? I was having dinner with my friends.
2.过去进行时（背景）+一般过去时（背景下发生的短暂动作或状态）The phone rang while i was having my bath, as usual. I was watching TV when the phone rang.

### 将来进行时（will be doing）
1.将来某特定时刻活动正在进行 I'll be lying on a beach in Santa this time tomorrow.Don't telephone after eight tomorrow.I'll be having a meeting.

### 名词
名词短语（名词与它前面的修饰语）These red roses are for you. I have three close friends. I really need a new computer.
1.功能 主语 宾语（介词不能单独使用，后面所接宾语）宾语
2.修饰语 限定词：泛指，特指，定量，不定量（these/three/a/the/my/that）,冠词（a/an/the） 形容词：red close new best small
3.位置 限定词在形容词前：限定词+形容词+名词:three red roses.

### 名词可数与不可数

### 名词分类
专有名词 Paris, the United States,Bill Gates 普通名词 可数名词 个体名词 student tree hospital house piano 集体名词 team committee police group family 不可数名词 物质名词paper water cotton air 抽象名词 birth happiness evolution technology hope 简单名词 story student teacher 复和名词 grilfriend roommate mother-in-law

### 是否可数的相对性
例如paper I need some paper to write a letter(纸，不可数) I have a term paper to write on weekends(论文 可数) I bought a paper(报纸 可数) room（空间 不可数；房间 可数）

### 可数名词与不可数名词比较
可数名词：前面可以+a or an or 数词（two） 不可数名词：不可+a or an or 数词（two）

### 不可数名词
1.物质名词不可数 beer blood coffee cream gasoline honey juice milk oil tea water wine bread butter cheese ice ice-cream meat beef chicken fish chalk copper cotton glass gold iron air fog oxygen smoke
2.抽象名词 advice anger beauty confidence fun happiness health honesty information love lunch peace
3.总称名词不可数 furniture fruit jewelry luggage equipment poetry machinery

### 不可数名词的度量
1.piece advice bread baggage chalk equipment furniture information jewelry luggage music news
2.bottle/cup/drop/glass beer blood coffee milk tea water wine（葡萄酒）
3.otherwise a loaf(一条) of bread/a tube(管子) of toothpaste（牙膏）/a pack of cigreatte/a slice of meat

### 名词所有格
1.单词名词后+'s
2.复数名词后+s' or复数型名词后+'s her friends' money the children's Day
3.复和名词后+'s my father-in-law's company everyone else's viewpoints Henry the Eighth's wives the President of America's secretary(秘书，干事，文书，部长）.
4.and连接的并列名词：公有情况：最后+'s;各自所有情况：每个名词后+'s
5.重量 度量 价值 two pounds' weight/a ton's weight/a ton's steel/two dollars' worth of sugar
6.省略 
1）前文以出现，避免重复 This bike is mine,not Michael's
2）表示店铺或教堂（套加the）at the baker's/at the butcher's/at the chemist's/at the doctor's
3）人名后的所有格省去名词表示住宅 go to my sister's/I called at my uncle's yesterday
7.of所有格的其他关系
1）主谓关系 the visitor's departure/the teacher's requesy/the growth pf agriculture
2）动宾关系 the children's education/the boy's punishment/the discussion pf the plan

### 人称代词

### 主格人称代词
作用：主句或从句中做主语 She is my daughter. It was he who helped me when i was in trouble.顺序：you,he,i;we,you,they(I总是放在最后)

### 表示泛指的主格代词
one:任何人，包括说话人 One is knocking at the door.(错误，说话人不算，所以不能用one) Somebody is knocking at the door. One后面使用的代词，美国一般用he,him,himself,his. We/You/They可以表示泛指：人们They say=People say or lt is said They say it is going to be a cold winter.

### she（her）的拟人化
表示country,motherland,moon,earth,ship The ship lost most of her rigging in the storm

### 宾格人称代词
作用：做宾语（也可做表语）I like her.Who is it? It's me.注意：做表语时，后面跟定语从句时，需要用主格人称代词。It was he in whom we had the greatest faith(he在从句中做介词宾语)主格和宾格壬辰代词可以做同位语：We teachers should be patient with students.Our teachers are all nice to students.

### 做宾语时的位置
直接宾语前：He bought me a pen as birthday gift.直接宾语后：He bought a pen for me as a birthday gift;I've lent much money to him.若直接宾语是人称代词，只能置后，但是不适用于不定代词：I will give it to you.I'll show you something;I didn't give Rex any.在短语动词中间：Hand them in; throw it away;pick it up.若是名词，则中间和后边都可：hand your papers in=hand in your papers.

### 反身代词
必须主语宾语为同一人时，做宾语：God helps those who help themselves.强调主语:He himself went to visit the old lady（他亲自去看望那个老太太的）不产生歧义下可置后：He went to visit the old lady himself.He spoke to the boss himself.(有歧义)
强调宾语：反身代词在宾语后：He saw Tom himself(他看到了Tom本人了) I will send this gift to John himself(给John本人，不是通过转交) 介词+反身动词 by oneself：独自一人地 I went there by myself(我自己一个去了哪里) I went there myself(我亲自去了那) of oneself：(自动地) The door opened of itself(们自动地开了)

### 介词
特点： 不能单独使用，后面需携带宾语（名词，代词，数词，动名词，动名词短语，名词从句）
搭配：在介词前的词：动词（depend on） 名词（pay attention to) 形容词（ke kind to）
充当：与其宾语构成介词短语后可充当主语，补足语，定语，状语
作用：词与词之间的表示关系
种类：简单介词（at,by,for,from,in,near,of,off,on) 复和介词 简单链接：inside,into,onto,out of,outside, throughout,upon,within,without 
搭配连用：as to, from above,from behind,from beneath,from under, until after.

### 介词短语
at the cost of, at the mercy(仁慈) of,at odds with, by means of, by reason of, by virtue of,by way of, in place of, in favor of, in spite of, with an eye to(着眼于，试图).
关于：in/with reference to,in/with respect to, in/with regard to

### 时间介词
at, in, on
1)at(时间点) 
    a.特定时刻：at nine after ten
    b.不确定时刻：at night,at dawn,at midnight,at that time,at the moment,at Christmas
    c.at the age of eight/at eight He got married at twenty
2)in(时间段)
    a.长时间段 in the morning/afternoon/evening in spring/summer/autumn/winter in the past,in the past ten years in the twenty-first
    b.在时间之内/后，表将来时He said he would come back in a month. Tha train is leaving in a minute
    c.in+动名词：在做...过程中 In crossing the river, we caught some fish. In working, we can learn a lot.
    d.几月：In October
3)on(表示具体时间)
    a.具体日期和星期 On Monday On the birthday
    b.特定某天上午，下午 On the night of December 31,1999 On the eve of Christmas/New Year On a hot midnight in July
    c.在第几天 On his first day to school.On the tenth day i was in Beijing.
    d.on+动名词 or 名词=as soon as
    e:On hearing the bad news, she burst into tears.On arriving, i came direclty to visit you.The first thing i did on arrival of Beijing was visit him.
from 和to或till/until连用Most people work from nine to five.
since 和时间点连用，从那一时刻起。现在完成时，过去完成时连用 He has been here since last Sunday I haven't seen him since two years ago.I haven't seen him for two years It's two years since i last saw him.

for 和时间段连用，表示动作延续到说话的那一刻。现在完成时，过去完成时连用 I have lived here for a year I have lived here since this time last year.
by 
a.no later tahtn:不迟于某个时间，到了某个时间 by the end of next year
b.by引导时间状语常与将来完成时or过去完成时连用By the end of next year i'll have learned 2000 words By the end of last year I had learned 2000 words.

时间介词短语
at the beiginning of 在...的开头 at the beginning of a book there is often a table of contents.at the beginnng of the concert.at the beginning of January.
in the beginning: at first=in the early stages in the beginning,i wrote to my family regualrly.later i just gave up
at the end of But at the end of this process,unfortunately,the students are none the wiser.At the end of a book there may be an index At the end of the concert At the end of January
in the end: eventually=at last:最终... Jim couldn't decide where to go for his holidays.He didn't go anywhere in the end.
有last/next/this/every不再加介词 i'll see your next Friday.
during for  during在...期间内，强调这时间内发生了什么 for表示延续时间的长短 I had lived the contryside for 8 years before i moved to Beijing.I studied in this university for 4 years.During that time most of my time was spent in learing English. My father was in hospital for six weeks during the summer.
during接表示一段时间的名词：stay,visit,travel During my visit to China During the travel to the south During the Middle ages
for+时间段 for six years for two months for ever for two hours.(6年了)
### 方位介词
at,in
at（小地方）at home,at the office,at school,at the bridge,at the crossroads,at the bus-stop at the doctor, at the hairdresser
in（表示大地方）in a country,in a town, in a village,in the street, in the forest, in a field, in a desert
其他情况（固定搭配）in a line/in a row/in a queue  in a photo/in a picture in a mirror in the sky/in the world in a book/in a newspaper/in a magazine/in a letter in the front/back row (at the front/back) in the front/back of the car at the front/back of the building/cinema/classroom
### 谈论建筑
at 表示事情发生的场合 I met him at the cinema last night.
in 表示建筑物本身 I enjoyed the film but it was very cold in the cinema
in强调在建筑里，at包括建筑物周围及里面 at the restaurant(可以是餐馆内，也可以是在餐馆附近的某个地方) in the restaurant（在餐馆里） at the cinema（在电影院，不一定里面）
### on,over,above
on表示两者接触 Put away the books on the desk.

on的其他情况 on the left/on the right on the first/second floor on a map on the page

over不仅接触，还有覆盖的含义 Spread the cloth over the table

over还可以表示正上方 There is a bridge over the river

above仅表示上下位的关系，不接触，也不是正上方 The sun rose above the forizon

### below,under,beneath
under在下方，可接触，可不接触 I put the money under the mattess

below表示两个表面之间间隔距离 They live below us

beneath可以替换under,但是偏向抽象含义 He would think it beneath him to tell a lie  She married beneath her

### 手段介词
by
### 表示行为方式
send something by post
### 表示交通工具
by car,by train,by plane
### with
表示用具体的工具做某事 
I killed a fly with a fly-flap（苍蝇拍）
in
表示某种方式做某事
Write in pencil
### through
by接近，through一半多根名词连用，by多根名动词连用 hey talked to each other through an interpreter.

### 动名词

### 做主语

### 直接句首做主语：谓语动词用单数
a.Seeing is believing
b.Reading is like permitting(准许) a man to talk a long time,and redusing you the right to answer.
c.Having a successful marriage takes effort and patience,and communication is the key.

### It is no good doing sth句型
it形式主语，真正主语是doing sth no可以替换为：any/some good, any/some/no use, a waste of time. 
a.Is it any good trying to explain?
b.It's not much use my buying salmon(n.	鲑; 大麻哈鱼) if you don't like fish
c.it's simply a waste of time and money seeing that movie.

### there be句型
1）there is no point (in) doing something 做某事没有意义
a.there is no point in my going out to date someone,i might really like if i met him a the time,but who,right now.has no chance of being anything to me but a transitional man
2）this is no use/good(in) doing something
a.there is no use your arguing with him
b.there is no use your complaining to me about this.
3）there is no doing something=it's impossible to do something=we can't do something.
a.there is no gainsaying/denying the fact that...母庸质疑
b.there is no telling what will happen tomorrow.
c.there's no knowing the future=it's iimpossible to know the future,or we can't know it.

### 做宾语
一些动词后只能用动名词做宾语
appreciate(欣赏),avoid,consider,delay,dislike,enjoy,escape(避开),feel,like,finish,can't help,involve(包含)，overlook,permit(允许),postpone(延期),practice(实践),risk(危险),can't stand,suggest,tolerate,understand
a.i will overlook（忽略，俯视） your being so rude(粗鲁的) to my sister this time but don't let it happen again.
b.Many of the things we do involve taking some risk in order to achieve a satisfactory result.
c.Being a bad-tempered man,he would not tolerate having this lectures(训斥) interrupted

### 接动名词和不定式有区别的动词
很多动词接动名词和不定式均可，但是意思有很大差别。
demand,deserve,need,require,want
动名词：主动形式表示被动 不定式：必须用被动形式
remember,forget,stop,go on,regret(感到遗憾)
动名词：表示发生于这些动词之前的事 ​ 不定式：表示发生在这些动词之后的事

### remember
remember doing sth: remember/recall something that happened in the past. ​ 记得已做过某事 ​ a. I still remember being taken to Beijing for the first time. ​ b. I don't remember/recall locking my suitcase ​ =as far as I know, my suitcase should be open ​ remember to do sth: remember to perform a responsibility, duty or task. ​ 记得需要履行的职责 or 任务。 ​ a. Remember to go to the post office, won't you? ​ b. Remember to do some shopping after work. ​ c. Clint always remembers to turn off the lights when he leaves the room.

### forget
forget doing sth: forget something that happened in the past. ​ 忘记了已做过的某事 ​ I forgot locking the door. So when I came back, I found the door locked. ​ as far as I know, the door should be open. ​ forget to do sth: forget to perform a responsibility, duty or task. ​ 忘记要做的事 ​ As well as getting on everybody's nerves, he's got a habit of borrowing money and forgetting to pay it back.

### stop
stop doing: 停下经常做的或手头正在做的事情 ​ I really must stop smoking. ​ stop to do: 停下来去做某事 ​ stop to have a rest.


### go on
go on doing sth: 继续做一直在做的事情。 ​ a. The teacher went on explaining the text. ​ b. Peter went on sleeping despite the noise. ​ go on to do sth: 改做另一件事 ​ a. He welcomed the new students and then went on to explain the college regulations. ​ b. Finishing the new words, the teacher went on to attack the text.

### regret
regret doing sth: regret something that happened in the past. ​ 对已发生的事情感到遗憾 ​ a. I don't regret telling her what I thought, even if it upset her. ​ b. I regret letting slip that opportunity. ​ c. I regret lending him so much money. He never paid me back. ​ d. Now he regrets not having gone to university.

regret to do something:regret to say,to tell someone,or to inform someone of some bad news 遗憾的告诉或者通知某人某个坏消息。
a.We regret to inform you that we are unable to offer you employment.
b.I regret to tell you that you failed the test.
c.We regret to inform you that the flight has been canceled.

### 动名词的其他结构
have difficulty (in) doing th trouble problem (a lot of fun) (lots of) pleasure a hard time a good time a difficult time
注意：take the trouble to do sth, trouble to do sth,have(no) time to do sth.
a.I worked sos late in the office last night that i hardly had time to catch the last bus.
b.I have a hard time getting used to living in a big place.


an't help doing, can't resist doing, can't keep from doing, can't hold back from doing ​ can't keep back from doing ​ 注意: can't help but do, can't but do, can't choose but do, etc. ​ No one can help liking Tom；he is such a cute(可爱) boy.

be worth doing值得做;主动形式表示被动be worthy of being done 或be worthy to be done.
a.The book is worth reading.
b.The book is worthy of being read.

### 动名词的复和结构
物主代词（his，my,your等）所有格名词(Mary's Tom's)与动名词连用，即构成动名词的符合结构。用来引出动名词的逻辑主语，以区别于句子主语。Clint insisted on reading the letter. Clint看了信） ​ Clint insisted on my reading the letter. （我不得不看信） Would you mind telling us the whole story? （你告诉） ​ Would you mind Tom's telling us the whole story?=Would you mind if Tom tells us the story ​ He disliked working late ​ He disliked my working late ​ I object to making private calls on this phone ​ I object to his making private calls on this phone.

### 用法
做主语或宾语
a. Tom's coming home at liat was a great consolation.（做主语）
b. Do you ming my making a sugguestion?(做及物动词宾语)
c. Our discussion of earthquakes would be incomplete if we didn't raise the possibility of their being caused by external forces.
物主代词（his)可以改为宾格代词（him）or所有格名词（Tom's）改成普通格名词 （Tom）
a. It's no use Tom arguing with his boss. ​ b. Do you mind me making a suggestion? ​ c. I am annoyed about John forgetting to pay.

应用原则： ​ 1) 若动名词复合结构在句中做主语，最好用所有格形式 ​ a. Tom's refusing to accept the invitation upset me. ​ b. His refusing to accept the invitation upset me. ​ c. It was a great consolation his coming home at last. ​ 2)动名词复合结构在句中做宾语时候，用普通格或所有格均可 ​ a. Do you mind me making a suggestion? ​ b. I am annoyed about John forgetting to pay.

### 动词不定式

### 做主语

### It+to do sth句型
1.It+to do sth句型；it做形式主语
a.It is easier to spend money than to make money.
b.It takes time to study English well.形式主语it不能用this或that替换
This is impossible for people to stare directly at the sun.应用it

2.不定式置于句首做主语，谓语动词要用单数。
a.To err is human;to forgive,divine
b.To solve this problem takes a genius like Einstein.
c.To love for the sake of being loved is human,but to love for the sake(缘故) of loving is angelic
d.To send a letter is a good way to go somewhere whithout moving anything but your heart.

### 做宾语

### 动词+to do
1.直接跟在一个及物动词后面做宾语 
特点一：句子的主语和不定式的逻辑主语是一致的，动作都是由助于发出。
特点二：这时句子谓语动词多是描写态度；
a.I hope to see you again.
b.This company refused to cooperate with us.
c.He promised not to tell anyone about it.

接不定式做宾语的动词有agree,appear,beg,begin,dare(挑战),decide,expect,fail,forget,happen,hate,hesitate,hope,intend,like,love,manage,mean,prefer,prepare,pretend(佯装),promise,propose(建议),
refuse,regret(遗憾),remember,seem（似乎）,start,swear(咒骂),try,want,wish

2.连接词引导宾语从句的简略形式：动词+连接代词or连接副词or连词whether+to do
a.I wonder(想知道) who to invite.（= who i should invite)
b.Show us what to do.(= what we must do)
c.I don't know whether(是否;) to answer his letter.(= whether i should answer)类似动词还有：ask,consider,decide,discover(发现),explain,forget,find out,guess,imagine（设想）,know,learn,observe,remember,see,tell,teach,think,understand,wonder

### 做宾语补足语

### 动词+ sb to do sth
1.通常结构：动作+sb to do sth
    a.They don't allow people to smoke in the theater(剧场).
    b.The chairman(主席) declared the meeting to be over.
    c.Allow me to drink to your success.
    d.Allow me to propose a toast to our friendship.
    f.My mother wishes me to return to China.
  常用动词：advise,allow,ask,beg,cause,enourage,expect,forbid,force,get,help,like,order,permit,persuade(劝说),remind（提示）,
  teach,tellwant,warn,wish(hope不可)

2.在let,make,have,see,hear,feel,watch,notice,listen to等动词后面，不定式做宾语补足语to要省略，改为被动语态，则必须带to(详见 不带to的不定式)

### 作定语

### 名词+to do sth
1.动宾关系 被修饰名词的逻辑上做不定式的宾语
    a.She has four children to take care of.
    b.I had no place to live in.
    c.Your just regard me as a thing,an object to look at,to use,to touch,but not to listen to or to take seriously.
    d.I gave the kid a comic to read.
    e.He needs a place to live in.
    f.I have no partner to speak English with.
    g.I need a pen to write with.
    h.I need a piece of paper to write on.
注意：1.不定式一般不用被动形式
     2.不定式动词后面不能再加宾语
        a.I gave the kid a comic to read it.
        b.I need something to eat it.
     3.不定式动词后介词不省略
        a.I have no partner to speak English
        b.I need a pen to write.
     4.被only,last,next,序数词，最高级修饰的名词通常用不定式做定语
        a.I don't think he is the best man to do the job.
        b.The next train to arrive is from New York
        c.Clint was the second person to fall into this trap(牢笼)

### 做独立成分
修饰整个句子：to begin with,to tell the truth,to make a long story,so to speak,to be brief(短时间的)/exact(精确的)/frank/honest(诚实的),to say nothing of(姑且不所说)，to say the least(至少可以这么说)
a. To begin with,on behalf of(代表) all of your American guests,I wish to thank you for the incomparable hospitality.
b. I have a point there,to say the least.
c. To make a long story short,he is in hospital now.

### 作状语
  
目的在状语or结果状语

做目的状语
a.Hating people is like burining down your own house to get rid of a rat.
b.To avoid criticism,do nothing,say nothing,be nothing.
c.To acquire knowledge,one must study;but to acquire wisdom.one must observe.
d.We had better start early to catch the train.
e.I went to the post office to mail a letter.

可以用in order to do或so as to do强调目的状语
c.I quote others in order to clearly.

The teacher raised her voice in order for us to hear more clearly. ​ 注意：so as to不放在句首；to do和in order to do可以放句首。

做结果状语
1.直接做结果状语
a.He lived to be a hundred years.
b.What have i done to offend you？
c.He lived to see secound world war.（= he lived until he saw world war II）
### never to do表结果 ​ 
a. John left his hometown ten years ago, never to return. ​ b. We parted never to see each other.
### only to do引出意想不到或不愉快的结果 ​ 
a. We hurried to the railway station, only to find the train had just left. ​ b. All too often, women complain that they're educated as equals, only to go out into the workforce to be treated as inferiors. ​ c. He worked very hard, only to find he had not finished half of the job.
### enough to ​ 
a. He is not old enough to go to school. ​ b. The teacher speaks loudly enough to make himself heard clearly.

### too...to结构：太...而不能 ​ 
a. The box is too heavy for me to even move. ​ b. The tea is too hot to drink.

### 形容词
1.句子主语和不定式可以构成逻辑上的主谓关系 这类形容词通常表示人的性格特征或行为表现brave,careful,careless,clever,considerate,cruel(残酷),foolish,generous,kind,modest,nice,polite,rude(粗鲁的),selfish（自私的）,silly（愚蠢的）,stupid,thoughtful(沉思的)
  a. He was surprised to learn how much he had spent. ​
  b. The boy was careless to break（破坏）the window.
句子主语和不定式构成逻辑上的动宾关系
a. She is interesting to listen to=It is interesting to listen to her. ​b. Relativity theory isn't easy to understand=it isn't easy to understand relativity theory. ​ 
c. She is very nice to talk to=It is very nice to talk to her. ​ 
d. Mary is easy to get on with=It is easy to get on with Mary. ​ e. English is difficult to speak. ​ 
f. Football is very interesting to watch. ​ 
g. Barbara is interesting to listen to because she reads a lot. ​

​在动宾关系的情况需要注意： ​ 
1)不定式动词不用被动式（最容易出错）
    a. English is difficult to be spoken.
    b. Football is interesting to be watched. 

2)不定式后不加宾语 ​
    a. Football is very interesting to watch it.​
    b. She is nice to talk to her.
3)不定式动词所带介词不能省略 
    ​a. She is interesting to listen.
    b. She is easy to get on.

It's impossible for fish to live without water.
it's necessary for students to do more exercise in learning English.
The boy was made to sing the song once again.
He couldn't help bursting into tears after he heard the news.
She could not but admit that they were justified in this
They forbade him to go to the park
The first explorer to reach California by land was Strong Smith, a trapper who crossed the southwestern deserts of the United States in 1826
He was the first to arrive and the last to leave
The teachers don't know what it takes to start and run a school.

### 动词的复合宾语中to省去
1. 感觉动词：see, hear, watch, notice, feel, observe ​ 
2. 使役动词：let, make, have ​ a. The teacher has us write a composition every week. ​ b. I saw a man enter the shop. ​ 但为被动结构时，后面需要+to ​ a. A man was seen to enter the shop.

### 一些短语中to省略
had better, would rather, would sooner, would just a soon, might（just） as well, cannot but ​ cannot choose but, cannot help but ​ a. I cannot but admire his courage. ​ 
b. We might as well put up here for tonight. ​ 
c. I couldn't help but fall in love with you.

### do nothing/anything/everything but do省略
a. I have nothing to do but wait. ​ 
​b. I have no choice but to wait（but前没有do，则不定式+to）
c. He needs nothing but to succeed. ​ d. He will do anything but give in ​ 在解释do的精确含义的名词从句和定语从句做主语的句子中，be动词后直接+do ​ 
e. All that I could do then was wait. ​ 
f. What I could do then was wait. ​ 
g. All you do now is complete this form. ​ 
h. No mountains too high for you to climb. All you have to do is have some climbing faith. ​ No rivers too wide for you to make it across. All you have to do is believe it when you pray.

### 被动语态

#### 构成：be +过去分词
1.实施者明显 ​ 
    a. The rubbish hasn't been collected. ​ 
    b. Your hand will be X-rayed. ​用X射线拍摄检查;
    c. The streets are swept every day.

2.实施者未知或没必要提及 ​ 
    a. The President has been murdered. ​ 
    b. My car has been moved. ​ 
    c. Rice is grown in many countries. 
​    d. The library was built in 1890.

3.泛指人们 ​ acknowledge, assume, believe, claim, consider, estimate, feel, find, know, presume, report, say, think. ​
    People believe him to be honest. 
    He is believed to be honest.

4.主句主语是one, you, they 通常用被动语态 ​ 
    One/You see this kind of advertisement everywhere. ​ 
    This kind of advertisement is seen everywhere. ​ 
    They are building a new public library in our town. ​ 
    A new public library is being built in our town.
5. 避免改换主语（可以接by短语） ​ When he arrived home, a detective arrested him. ​ When he arrived home, he was arrested.

不及物动词没有被动语态
He came here last night. ​ He looks fine.

双宾语可以有两种被动语态（人做主语更常见）
Someone gave me a gift. I was given a gift. ​ Someone gave a gift to me. A gift was given to me.

### 静态被动语态
Clint broke the window last night ​ The window was broken last night ​ Now the window is broken. ​

常见interested, excited, satisfied, married, disappointed, scared, frightened, worried, lost ​ a. I am interested in grammar. ​ b. I am satisfied with Clint's grammar course. ​ c. She is married to her teacher. ​ d. The table is made of wood. ​ e. Are you scared of snake?

### get与被动语态（get与过去分词连用）
可以构成被动语态
a. My watch got broken while I was playing with the children. ​
 b. He got caught by the police because he exceeded the speed limit.

### 可以接静态的被动形式，表示主语的状态
a. I stopped working because I got tired. ​ b. I got worried because he was two hours late. ​ c. She is getting dressed to the party and has trouble deciding what clothes to wear.

### have/get sth done

have sth done或者get sth done
安排别人把事情做好（主要用法） ​ a. Are you going to repair the car yourself? ​ b. No, I'm going to have it repaired. ​ c. I want to have/get my items repaired. （My items need repairing.） ​ d. I must get my hair cut=I want someone to cut my hair. ​ e. You should get/have your bike repaired. ​ g. If you don't get our of my house, I'll have you arrested.
意外或不行的事情 ​ a. I got my car stolen last year. ​ b. Have you ever had your passport stolen? ​ c. Joe had his leg broken in a fight. ​ d. It took me two hours to get the washing done. ​ e. Don't get your plans changed.
