<!--
 * @Author: hua
 * @Date: 2019-12-01 09:46:41
 * @description: 
 * @LastEditors: hua
 * @LastEditTime: 2020-08-06 10:33:28
-->
### 作文
一 
---
SCENE(场景)
You have read the following advertisement put by an American couple.

Student: a 14-year-old boy from from USA,having stayed in China for three month.  
Tasks: tutoring(辅导) in math and physics, practicing(练习，工作) Chinese.  
Goods level of English required
Contact us at smiths@gmail.com  
Now you want to get in touch with the family.Write an email to the couple,telling them about:  
1) your interest in the job;
2) your qualifications for the job;(胜任)
3) your time available for the job;(有空)

---
You should write about 100 words. Do not sign your own name at the end of your email, Use "Wang Lin" instead.  

Dear smiths  
&emsp;My name is Wang Lin,I'm a teacher.I'm 28 years old, I like play basketball and write english document.I like to get alone with children;I good at English, math,physics and so on.</br>
&emsp;I often communicate using English with my students.During these three months,I have lots of free time, so I can use it to tutoring young children;My house have fee room,So you children can live together with me. I think this job is very fit for me.So I choose to apply for this job.If you think I can, Please reply me.Thank you.
<div style="text-align: right">Best regards</div>
<div style="text-align: right">Wang Lin</div>
<div style="text-align: right">wanglin@gmail.com</div>

二
---
Setting up a national long-distance educational system in China to provide educational opportunities via Internet.

---

&emsp;Lots of poor people can't go to school at west in China.Because education is expensive and concentrate in the south major cities. This is A chance for them to study at the Internet if we has a national long-distance educational. Internet is a great invent,It can people and people exchange many information at the different area. Education can change people mind world view.</br>
&emsp;All the Educators can timesaver and has lots of time to research new things, All the poor people need not go to south city far away,They can study in the their house,Only need a computer and network. at the end,I think a national long-distance educational will be popular in the China. Because china is a big country and have lots of poor people can not study.


三  
___
You work for a foreign company.Now you want to ask for a short period(一段，时间，时期) of leave.Write an email to your boss Mr.Smith to tell him.  
1) why you want to ask for leave;
2) how long you want to be away;
3) what you plan to do about my your current work;

---
You should write 100 words. Do not sign your own name at the end of your email.Use "Wang Lin" instead.

Dear Smith   
&emsp;I want to leave company for a short time, because i have some troubles from my family;My mother was injured her leg when she was cooking in the kitchen room; I need to take care of my mother in the hospital during two weeks from Otc.8 to Otc.22. </br>
&emsp;I will prepare(准备) turn over(移交) my work for my colleague(同事)—— David. I will finish my all work things at this week. During i leave time, If you have a serious problem, you can let David contact with me by phone, I will go to company at once. Please reply for my apply at this week.Thanks a lot.
<div style="text-align: right">Best regards</div>
<div style="text-align: right">Wang Lin</div>
<div style="text-align: right">WangLin@gmail.com</div>


四  
___
Younger citizens are finally favoring the beauty of the natural word over the cries of amateur singers.
___
&emsp;At 21 century, Most of younger people were studied in the high school.They have enough knowledge, correct world view and love natural word.But people do not have enough time to enjoy the life, Either people are need going to work or going to school to study lots of subjects
from 8:30 am to 17:00 pm. During the week, People are hope comfortable themselves.Best some ways is go to park, climb mountains(爬山) and so on,Because it can let people feel relax,To escape the hustle and bustle of the city(躲避城市的喧嚣).</br>
&emsp;Environments is real exist, When people keep the plants in the home, The plants can not keep health longer,
Because they are grow up need animal to carry their seed and harm can promote body to be stronger than before. so,
I think people need to come back to real exist.

五
___
You fond some problem with the book you bought from a US online bookstore.Write the bookstore an email to let it know.  
1)when you bought the book;  
2)what problem you found with the book;
3)what solution you expect.  
You should write about 100 words. Do not sign your own name at the end of your email. Use "Wang Lin" instead.

Dear bookstore  
&emsp;I bought many math subject books in your US online bookstore at Otc.9;i found the books was broken and  have lots of error information in it when i received it.I am very angry about this.I think you need to repay me for 50 dollars for instead and replace it at once;if you disagree my expect,i will complains your company.Please reply my email at this week.Thanks a lot.
<div style="text-align: right">Best regards</div>
<div style="text-align: right">Wang lin </div>
<div style="text-align: right">WangLin@gmail.com</div>

六 
___
A local environment group is recruiting volunteers at the moment. You are willing to become one of them. Write an email to the recruiters, telling them.  
1)why you want to be a volunteer;  
2)what you can do for the group;
3)when you will be available.

Dear Environment Group
&emsp;Hello, My name is Wang lin, I am twenty years old, I like enjoying environment life with animals. So i want to join environment group to be a protect animals volunteer.</br>
&emsp; When i was little, I often live alone with my dog at home,My pet dog name is Wang cai, i like playing throw ball game with it, it always run faster than me and like to lick(舔) my hands. Now i grow up to be a adult,
i want to be a protect animals volunteers. I can called(呼吁) people protect animals and plant. I am a programmer,I have free time at weekend, so i can go to your company at weekend. If you want to send offer to me,please reply me at the week. Thank a lot.
<div style="text-align:right">Best regards</div>
<div style="text-align:right">Wang lin</div>
<div style="text-align:right">WangLin@gmail.com</div>

七
___
rich and normal people have different mind for do somethings
___
&emsp;Rich people often use lots of money to improve(提升) themselves. Actually, they are think knowledge is very expensive. But normal people use lots of time and hurt health body to make money. So i think they are have different mind because of life environment.
&emsp;When i was little, I want to be science. But My family is very pool, they are want to make lots of money but don't like to study. They are not have good world view, knowledge and how is love, so they are can not hold out(守住) money any longer. So i was walked lots of circle life road and to be a programmer, Now i think study knowledge is important and very valuable(有价值). </br>
&emsp;At the 21 century, Lots of people can study information at the Internet, We can  communicate(沟通) easily than before. I want to poor people can use more times to study knowledge instead of make money. When you have knowledge, you can look different world at the life. 

八
___
Life is full of accidents, people will always escape, overcome the fear after the will success will make themselves proud.
___
&emsp;Lifetime have lots of accidents and chances, people always can not hold it when it come. Success people stick to do themselves believe thing, never mind fail and poor.
&emsp;When i was a little children, i want to be a scientist but i don't use enough time to learn about physical knowledge and hate to do difficult things until my twenty-five age. I think i can not continue enjoy comfortable life, i need to change my life, i am start learn program knowledge every day and write my question at my book. My chance come from a company, it have not enough money to use a experience programmer and it want to give me a chance to work.Now, at 2019,Dec,twenty-nine. I am a four years experience programmer and i enjoy my work.
&emsp;Accidents often meet with our, we need to resolve it instead of escape. 
九
___
Write a letter to her (Andy Lewis), telling her about:
1) your family;
2) your schooling or work;
3) your hobbies;
___
Dear Andy Lewis:
&emsp;I am Wang Lin, i am a programmer. I look you want to find a pen-friend at the magazine advertisement, i also want to it.   
&emsp;I family member have father, mother, grandmother, elder(年长的) sister and me. I work in a Internet company at the WuXi city. I like learning English language, collecting bank commemorate(纪念) coins and so on, I think we can learn English and collect coins knowledge together. My Address is 302 ErQuan street, DongTing, XiShan, WuXi, JiangSu, China. I will wait for you reply litter. Thank a lot.
<div style="text-align:right">Best Regards</div>
<div style="text-align:right">Wang Lin</div>
<div style="text-align:right">WangLin@gmail.com</div>

十
___
electronic equipment effect people life  
___
&emsp;At 21 century, electronic equipment will be more and more. People will be dependent electronic even more. We need protecting ourselves health and alert electronic make people lost effort mind.
&emsp;When i was a kid, i always watch tv at bedroom and not have a good reading habits, then my eyes was be myopic eye, effected my face score and let me felled abasement. But it can let me know lots of knowledge. When i was a adult, i have a smartphone(智能手机).I often use it search about program knowledge at once when i need to unknown things. However i was be tried because of i use all my free time on it, i can not relax myself.
&emsp;At the end, i think the electronic has many vantage and disadvantage. we need keep awake mind to use and control them. It is really difficult things. 

十一
___
Suppose you are the secretary of the manager of a company. You attended the negotiation between your company and a foreign company. Write a memo of the negotiation. The memo should include:
1) time and place of the negotiation  
2) participants of the negotiation  
3) content of the negotiation including the agreement reached and differences that still remain
4) the time of the next negotiation
___
&emsp;My company will conduct of negotiation with foreign company at 10:00 in Beijing. Need to join member have general manager, personnel and finance people.
&emsp;This negotiation is about protect environment and how much spend on it, we will introduce my company protect environment experience and contribution(贡献). my company is  be responsible for reduce air harmful from power station.
&emsp;The next negotiation will be hold after two weeks, at Otc,19.I will tell them before three day at hold it.
<div style="text-align:right">Wang Lin</div>
<div style="text-align:right">Otc,5</div>

十二
___
Beijing has extended its ban on operating outdoor barbecues from the city center outward to selected suburbs, according to municipal authorities.
___
&emsp;Protect environment is very correct device for all of people. Environment can make lots of disaster(灾害), for example earthquakes(地震), typhoons(台风), storms(暴雨), droughts(干旱) and so on. Environment can take us many advantage, such as breeze, warm sun, cool water and so on.
&emsp;Lot's of people and group broken environment for make profit, they were can not care for environment and broken it.So government often check them and impose a fine, but many time we need to observe(遵守) behavior for yourself, because people perhaps don't know their action will broken environment. We need education children at school and hold meeting about how to protect environment.
&emsp;At the end, i think people and environment will be better than now.  

十三
___
 , but failed to keep it. Write a letter to your teacher. Your letter should include:  
1) apologize for your failure to keep the appointment.
2) explain your reason to your teacher. 
3) explain your wish to make another appointment.  
___
Dear Prof. Wang
&emsp;I cannot keep the appointment with you, Because i felt uncomfortable at the weekend; so i need go to hospital for an examination(检查) during a long time ,i cannot meet with you at the appointment time. 
&emsp;I'm really sorry about it, i think you will fell anger with it. so i will take a gift with you, i hope make new appointment at next monday. If you don't have free time, you can tell me you free time? 
&emsp;At the end, i want you replying my letter in the the week, thank a lot.
<div style="text-align:right">Best Regards</div>
<div style="text-align:right">Wang Ling</div>
<div style="text-align:right">WangLin@gmail.com</div>

十四
___
graduate expect salary often cannot arrive, foreign company need contact and communication with English language. private company use Chinese but salary always less than foreign company. English is very important position at the world.
___
&emsp;Many graduates from college to social for find a suitable(合适) job, but they are often hope to make lots of salary from work, however company want to suitable experience and good communication. 
&emsp; At the private company. People often use Chinese to communication with each other and work hardly, however salary only  three thousands - eight thousands, Lots of important job need use English language to read technology/science(专业) document and write email to foreign company.So a good English skill can let graduate get more money than before. English language is very important at the world. Because USA is a larger country at the world and lot's of people use English to contact and communication.
&emsp;Now, I am learning English and writing composition every weekend. I want to promote my English and program skill.
十五
___
1)thank him for entertaining you  
2)describe your feeling  
3)invite him to visit your hometown 
___
Dear Gao Jie  
&emsp;Hello, I am Wang Lin. I live in Shanghai. During the vacation(假期), i was gone to Beijing and thanks you for you and your family entertaining(招待), i like tasting you cooked dinner and playing electron game about protect plant.
&emsp;At my hometown, you can see lots of heigh building in the city and many people look like very busy.I want to invite you to visit my city and i will entertaining you and take you to visit famous spot(地点).Look forward to you letter.
<div style="text-align:right">Best Regards</div>
<div style="text-align:right">Wang Ling</div>
<div style="text-align:right">WangLin@gmail.com</div>  

十六  
___
how to distinction（分辨，识别）looks and competence(有能力)
___
&emsp;In the past, when i graduate from college. I am very 
confidence(信心) to find a creditable(体面) work, but i'm fail and sad about it. Until i attend to interview reach ten times. When interviewer answer this work need work hard and not have enough money to pay it, i also agree it because i don't have any way to find good job to instead it and i don't have profession(专业) knowledge to be quality.     
&emsp;Now, i am a programmer. When interviewer can not distinction my ability. I will show my productions and skills for it. I have lots of major experiences for my job.I through my hard to let people can know my ability,however looks. 
&emsp;At the end. I think many people want to be a competence man. But sometime people will fail and lost confidence.you only need insist study longer than before and try again. you will success.

十七
___
1) your age, gender, nationality
2) your possible educational background
3) your reason for getting the job
___
Dear Sir
&emsp;I am Wang Lin, twenty-five years old and i am  a male. I am graduated from Beijing Science College at 2013 and my major is English and manage, i was got English competition   champion(冠军).  
&emsp;I was looked this job at the magazine. I think my ability can qualify(胜任) this job, because i am good at operation computer skill and communicate with people, i need to get experience from this job.  
&emsp;According to the above(综上), i want getting the job. If you want to agree my think, reply my letter in next week.
<div style="text-align:right">Best Regards</div>
<div style="text-align:right">Wang Ling</div>
<div style="text-align:right">WangLin@gmail.com</div>  

十八
___
leftover man and woman increasing from China
___
&emsp;When i was a children, i live in a town with my family and they are very poor. My father was married with my mother at ninety years old. But i don't like this, because they are not have enough money and duty in their thoughts. I have a unlucky life with them.
&emsp;Now,i am an adult male. I feel lots of pressure from social and Internet. Leftover man need prepare(准备) many money for hold a big marry party. Leftover woman also want to marry to a rich and knowledge young man.  
&emsp; Generate leftover man and woman reason is education let people have higher worldview and enough goods can let them single life.This is a serious problem at the China.

十九
___
1) no smoking in the carriages. Smoking is only permitted in the smoking Area.  
2) smoking is not only bad for smokers" health but also bad for people around them.  
3) anyone who smokes in the carriages will be fined.
___
<div style="text-align:center;font-size:20px">No Smoking</div>  
&emsp;People smoking in the carriages is danger and harmful thing. Smoking will generate harmful gas and it is a fire source from people.So if you want to smoke, you need going to assign Smoking Area.   

&emsp;Smoking will either harm around people when you smoking or harm myself.Because when you smoking, it will produce harmful gas at once.The gas can not get away at once in the carriages.  
&emsp;According of the above. Let people go to appoint area smoke, it will let carriages to be safe and humanity.

二十
___
Take you hand, and hold it forever is rare(罕见)
___
&emsp;“Take you hand, and hold it forever" is rare a t all of the world. Lots of couple can not walk together forever. Because everyone have different temperament(脾气).  
&emsp;When you get married with your lover, at the start, couples will feel sweet each other. Next, they will lost sweet and bored each other, this is difficult process for themselves.If couples have knowledge and rich life, it will be easy to spend it. However, many couples are poor and be short of knowledge, actually, money can resolve almost problems at the real life.  
&emsp;At the end, couples temperament will disappear,they will walk to the end for both parties(当事人). About two percent people can do it. This is a rare thing for people.  

二十一
___
1) a description of the situation.  
2) complaint about the situation.  
3) your request
___
Dear Hotel Manager  
&emsp;I am Wang Lin, i was often lived in your hotel and felt warm and comfortable(舒适).However, nobody come here to fixed electronic and cleaned my room at the last 2 day.This is bad thing for me.  
&emsp;Perhaps, you don't know it, but this thing let me can not have a good rest(休息).I cannot concentrate my mind to work.I need you help, can you let worker to resolve my request at once? If you can't resolve this problems, i will complain you hotel and repay my money,Thanks a lot.
<div style="text-align:right">Best Regards</div>
<div style="text-align:right">Wang Ling</div>
<div style="text-align:right">WangLin@gmail.com</div>  

二十二
___
Charlotte Elizabeth Diana is perfect name for their lovely princes.
___
&emsp;Charlotte Elizabeth Diana is a meaningful name for British and they family. Name is bailment（寄托）parents hope and love.  
&emsp;At the British, royal babies need assume(承担着) millstone when they are broth at the world. For example, when baby been a adult, citizens will respect them and need they are help at the helpless and hungry.Perhaps, war begin to harm them country and citizens, they will stand up and kill invader(侵略者). A good name will take luck and hope for a country.  
&emsp;According of the above. Diana's spirit lives on to British citizens and royal members. It will take lots of meaningful things for them.I think it is very perfect for baby.




























































































































































































































