<!--
 * @Author: hua
 * @Date: 2020-03-20 16:07:03
 * @description: 
 * @LastEditors: hua
 * @LastEditTime: 2020-03-20 16:33:36
 -->
```
defends: v.辩解
claim:v.宣称
violation：v.违法
dispute:v.争论
legal：adj.法律的
practioner:从业者
sworn: v.发誓
uphold:v.支持
constitution:n.宪法
course:课程
analysis：分析
prior:先前
ruling:裁决   
precedent:判例
period:一段时间
beside:相比
affair：事件
fundamentals:基础的
examination:考试
technique:技巧
college:大学；学院；学会
qualification:  资格；条件；限制；赋予资格
willingness：乐意
prove:提供
diagnose:诊断
recurrence:再发生；循环；重现；重新提起
estimate:估计
gene:基因
influence:影响
factor:因素
therapy:治疗
evidence:根据
detection:发现
consequently:因此
dramatically:显著的
monitor:关注
distribute:分发
symptom:病状
symbol:象征
```