<!--
 * @Author: hua
 * @Date: 2020-02-15 09:40:44
 * @description: 
 * @LastEditors: hua
 * @LastEditTime: 2020-02-15 09:40:45
 -->
### n.名词
```
noun的缩写
```
### v.动词
```
verb的缩写
```
### pron.代词
```
pronoun的缩写
```
### adj.形容词
```
adjective的缩写
```
### adv.副词
```
adverb的缩写
```
### num.数词
```
number的缩写
```
### atr.冠词
```
article的缩写
```
### prep.介词
```
preposition的缩写
```
### conj.连词
```
conjunction的缩写
```
### interj.感叹词
```
interjection的缩写
```
### u = 不可数名词
```
uncountable noun的缩写
```
### c = 可数名词
```
countable noun的缩写
```
### p1 = 复词
```
plural的缩写
```