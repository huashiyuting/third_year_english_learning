<!--
 * @Author: hua
 * @Date: 2020-01-31 11:26:30
 * @description: 
 * @LastEditors  : hua
 * @LastEditTime : 2020-01-31 13:04:27
 -->
```
soccer:足球运动
event:比赛项目
recently:最近
traditional:传统
cup:奖杯
ever:从来
seats:座位
sold:售出
exactly:确切的
evidence:证据
scientist:科学家
feet:脚
native:本国
Indians:印度
similar:类似的
spread：传播
material:材料
leather:皮革
stomachs:胃
accord：符合;一致;
state:国家
involve：参与
whole:全部
town:居民
seems:似乎
rely:信赖
certain:确信
routine：程序
practices:实践
labor:劳动
force:军队，力量， 促使，推动
shifts:转移，变化
beyond:超过，越过；晚于，迟于
daylight:日光
shift work:换班工作
express：表达
stress:压力
sense：感觉
conflict:抵触，冲突
demand：需求
maintain:保持
individual:独特
roles:角色
conventional:传统
sacrificing:牺牲
interview:采访
characterized:特征
wives:妻子
division:分开
provider：供养者
typified：典范
loyal:忠诚
expect:期望
out of:离开
manner:举止
sit back:向后靠坐
sense:会议
shit:垃圾
stable:稳定
companionship:友谊
leisure:休闲
implied:暗指的
adapt:适应
greenhouse:温室效应
effect:影响
cycle:循环
track:追溯
future:未来
silly:愚蠢
season:季节
light me up:让我开心起来
square:正方形
among:在...中
exciting：兴奋
wild:自然环境
camp:露营
inconvenient:不方便
structure:结构
grow:成长
autumn:秋天
wood:森林
white:白色
muddy:泥
threat:威胁
prediction:预言
excuse:借口
stick:粘性
cancel:取消
damn:该死
period:时期
defeat:失败
firm belief:坚定的信念
boyhood:孩童
since：数月
snowstorm:暴风雪
final：最终
since：因为
gone:消失
conquer:征服
climate:气候
weather：天气
pollution：污染
struggle：努力
call：称
against：反对
hate:讨厌
volleyball:排球
compete：竞争
tried:厌恶
impose:施加影响
natural:天生，自然
practice:练习
enjoyment:乐趣
quit:退出
lack of:没时间
athletes:运动员
turn:转向
average:平均
figure: 计算；认为；描绘；象征
mealtime:进食时间
pitfall:陷阱
decrease:降低
daily:日常
lead：导致
dramatic:急剧
benefits:益处
stroke:中风
ethnic:民族
blood：血
pressure:压
consuming：重要
advantage:优势
surprisingly:令人惊讶
African:非洲
suggest:倡议
reduce:降低
wide：宽的
conduct:进行
analysis:分析
determine：下决心
calculate:计算
emerging:出现
conclusion:结论
cut:挖
nationwide:全国性
accident:意外
annually:一年一次
avoided:避开
caused:发生
considerable:相当大
develop:发展
documented:文档
dramatically:戏剧
excess:过量
impact:作用
instances:实例
modest:不显著
revised:修改
slightly:略微
undertake:保证
```