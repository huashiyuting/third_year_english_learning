<!--
 * @Author: hua
 * @Date: 2020-01-29 19:12:46
 * @description: 
 * @LastEditors  : hua
 * @LastEditTime : 2020-01-29 20:20:19
 -->
```
cable:n 缆绳 vt 发电报  
hire:vt录用，雇佣 vi受雇  
unfortunate: adj 不幸的 n不幸的人  
flat: n 公寓 adj. 平的;单调的;不景气的;干脆的  
liable to duty:有责任纳税（应付税）
fault:n. 故障；[地质] 断层；错误；缺点；毛病；（网球等）发球失误  
whichever: pron. 任何一个；无论哪个；无论哪些  
extreme tiredness:极度疲惫  
nervous: adj. 神经的；紧张不安的；强健有力的  
tension: vt. 使紧张；使拉紧  
diplomacy: n. 外交；外交手腕；交际手段  
declare: vt. 宣布，声明；断言，宣称
acquired:vt 获得
momentarily: adv. 随时地；暂时地；立刻
at a loss：不知如何是好
attack：抨击
utterly: 出人意料的
rolex:劳力士
unnaturally:很自然的
ugly：丑陋
strange noise:奇怪的声音
confiscation:没收
failing to declare dutiable：未申报
examine：调查
quarter：四分之一
persuade:说服
contraband:走私货
luggage：行李
conclude:断定
custom：海关
notice：注意到
humorous:幽默
sarcastic:讽刺
solemn:严峻
matter-of-fact:事实
tone:. 语气；色调；音调；音色
puzzle:n 谜题
risky:危险
vulnerable:脆弱
turtle:乌龟
shell:n. 壳，贝壳；炮弹；外形
weakness:软弱
pay to hold back:值得隐瞒
play it cool:保持冷静
shown their hand first:先出牌
fair and equal:公平和想等
psychologist:心理学家
seldom:很少
struggles:斗争
imbalance：不平衡
rot:腐败
complete:完成
emerges:涌现
silent:无声
strongest:最强
confident：信心
withholding:扣缴
education:教育
therapist:治疗师
bored:使厌烦
depend:依赖
unfaithful:不忠
honest:诚实
intention:意图
come down:归结
sincerity:诚意
export:专家
passive:被动
the time of saying：说的时间
determination:决心
direction:用法说明,方向
electricity:n. 电力；电流；强烈的紧张情绪
union:联盟，协会；工会；联合
strike:罢工
since:自..之后
wage:工资
pity:遗憾，可怜
inconvenience:造成不便
blame:责备
inflation:通货膨胀
demand:要求
desperate:拼命
crisis:危机
uncontrollable:无法控制
impress:留下印记
hurts:损害
especially:特别
stand:立场
persuade:劝说
economy:经济
army:军队
power station:发电站
prison:监狱
suffer：遭受
live on：以..生活
shut:关闭
force:强迫
greedy:贪婪
what i say is:我说的是
trivial:不重要
wastes:浪费
taxpayers:纳税人
pop singer:红人
nurse:女仆
banned,ban:adj. 被禁的，被取缔的
nation:国家
security:安全
stability：稳定
goals:目标
demands:需要
suggest:建议
sectors:行业
relating:联系
thunder:雷
commonly：平常的
contact:联系
record:记录
series:系列
left of:剩下
gone:丢失
aware：知道
impact:冲击
widely:广泛
spread:传播
scholars:学者
fate:命运
tongue：vt. 舔；斥责；用舌吹
bomb:很多钱
tribe:族
surviving:活下来
species:种类
reservation:预定
extremely:非常
sense:感觉
tour:旅行
quick:快
science:科学
university:综合性大学
discoveries:发现
corridor:走廊
exhibits:展览
floor:楼层
distinguish:卓越的
physicists:物理学家
faculty:能力，教师
portraits:肖像
among:经过
universe:经验领域
clearly:显然
although:尽管
presence:存在
optimistic：乐观
circumstance：细节
presently：目前
realistic:现实的
virtually:实际上
```