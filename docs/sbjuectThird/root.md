<!--
 * @Author: hua
 * @Date: 2020-02-24 17:13:43
 * @description: 
 * @LastEditors: hua
 * @LastEditTime: 2020-02-25 17:12:38
 -->
### 词根
```
n 地震： seism/seismo变异
adj 地震的：seismic
n 地震学：seismology

词根radi-,-ray-,-rad-,-radio- = ray,beam 光线,辐射
拉丁文（radius(=ray)
1.radiant(闪耀的rad(=ray),-i-连接符，-ant形容词字尾)
adj.容光焕发的;辐射的
radiate v.放射；发散
radiation n.放射，发散
radiator n.散热器；电炉
radium n.镭 
radioactive adj.放射性的
You had the most radiant 
 
词根dyn,dynamo,dynam = power/ability 力，力量
来源于希腊语dynamis力量，该词根代表“力”的含义，属于物理学上的一个概念。所以词根构成的单词大多数属于物理学术语，如：dyne达因，dynamics力学。词根dynamo，dynam为dyn德变体，前者用于辅音，而后者则用于元音前，如：adynamic无力的；有异形同义词根：apt,ept,fore,fort,potent,viol,val,vail能力/力量 
同源词：
1.dynamic[dyn 力量+ -ic(a./n.)后缀->]
adj.有活力的，强有力的，动力的
n.动力，动力学
2.dynasty[dyn力量+ -ast最，-y(n.)名词后缀->一个国家力量最大的家族->]n.皇室，朝代，群体，家族
3.dynamo[dynam力量+ -o(n.)名词后缀->发电机是有力量发电的->]
4.dynamite[dynam力量 + -ite(v./n.)后缀+ ->炸药爆炸后的力量可摧毁整个桥梁->]
5.hydrodynamics[hydro-水 +dynam力量 + -ics(n.)表“...学”->研究水的力量的学问->] n.流体力学，水动力学
6hydrodynamic [hydro- 水+ dynam 力量 + -ic (a.) 形容词后缀，表示“…的”→]
  adj.水力的，水压的，液力的，流体动力学的

词根ac,acr,acid = sour(酸的)sharp(锐利的)
来源于拉丁语的分别来源于拉丁语acidus/acris/acutus/acuere酸/锐利/尖锐。-acri(d)-/-acer-为-acr-的变体，有异形同义词根：-acu-尖锐的;与-acid-相应的希腊语词根为：-oxy-酸，如：oxygen氧，oxylophytes适酸植物。
```