<!--
 * @Author: hua
 * @Date: 2019-10-27 20:59:32
 * @description: 
 * @LastEditors: hua
 * @LastEditTime: 2020-04-02 17:36:36
 -->
# 个人情况和人们

# 陌生单词
```
dialog:对话
introduce:介绍
outstand:突出
operation:操作
heard:听见
experience:经验
tomorrow:明天
college:学会，大学
appointment:约定
universal:环球 
trade:贸易
corporation:有限公司
extension:延伸
couple:夫妇
lady:女士
mister,sir:先生
except:除...，例外
distant:遥远
pocket:口袋
set up:建立
employees:员工
reported:报告
rags-to-riches:白手起家
rags:破布
riches:财富
forgot:忘记
follow:接下来
monologue:独白
famous:著名
rotten:腐烂
either:两者之一
worth:值
science:科学
scientist:科学家
inventor:发明家
printer:作家
philosopher:哲学家
musician:音乐家  
```

#### 2019.12.13
```
donkey:蠢，驴  
perhaps,maybe:可能
bachelor:单身汉
poor:平穷
rich:富
people：人
adult:成人
danger:危险
safe:安全
save:保存
children:孩子
torture:酷刑
environment:大自然
improvement:改善
avenger:复仇
repay:偿还
assemble:集结
clean:干净
wearing：精疲力尽
dirty：脏
duty：职责

```

#### 2019.12.23
```
stupid, foolish:愚蠢
just:刚好
adjust:协调,调整
util:龙套
tool:工具
hospital:医院
bathroom:厕所
across:横穿
along:沿着
special:特别
hurry:仓促
urge:督促
camera:照相机
cinema:电影院
living room:客厅
soft:放松
hard:努力
happiness:幸福
sadness:悲伤
vantage:优势
avenger:复仇
assemble:集结
arrive:到达
weight:重
detective:侦探
rat:老鼠
数字：one,two,three,four,five,six,seven,eight,night,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen,twenty,thirty,forty,sixty,seventy,eighty,ninety,hundred.
星期：monday,tuesday,wednesday,thursday,friday,saturday,sunday,week.
月份:January,February,March,April,May,June,July,August,September,October,November,December.
东西南北：east,west,south,north.
上下左右：up,down,left,right.
前后：before,after.
congratulation:恭喜
grade:年级
regards:问候
happen:发生
physics:物理
writer：作者
landlord:房主
master:主人
slave:奴隶
```

#### 2019.12.24
```
care:照顾
sensitive:敏感的
Christmas:圣诞节
Christmas Eve:平安夜
tire:累
expensive:昂贵
experience:经验
hand:手
leg：腿
eye：眼
ear：耳朵
nose:鼻子
mouth:嘴巴
```

#### 2020.1.11
```
assume:假设，认为
flaw:缺陷
another:另一个
invite:邀请
wallet:钱包
util:龙套
college:大学
level：等级
grade：年级
anyway:任何方式
identify:确认
equal：相等
refuse:拒绝
resist:反抗
against:反对
remember:记住
life：生活
around:周围
punch:一拳
blow：击，吹，打
cloud:云
windy:多风
who：谁
how:怎么
what：什么
which：哪个
can:能
possible：可能
must:必须
should：应该
before:以前
after:然后
subtract:减去
jacket:夹克衫
elder sister:姐姐
younger sister：妹妹
elder brother：哥哥
younger brother：弟弟
uncle:叔叔
aunt:阿姨
father:父亲
mother：母亲
grandfather:爷爷
grandmother：母亲
house:房子
live:住
hit:击打
cut：切
broken:破坏
cheat：欺骗
thief：小偷
```
### 2020.1.27 月份复习
```
一月：January, Jan
二月：February, Feb
三月：March, Mar
四月：April, Apr 
五月：May, May
六月：June, Jun
七月：july, Jul
八月：August, Aug
九月：September, Sep
十月：October, Oct
十一月：November, Nov
十二月：December, Dec
头发：fair
头：head
耳朵：ear
鼻子：norse
嘴巴：mouse
眼睛：eye
手臂：arm
手：hand
肩膀：shoulder
胸：chest
肚子：tummy
阴茎：penis
腿：leg
脚：foot
```

### 2020.4.2 
```
urban:城市的
rural：乡村的
urinate,pee:小便
shit,poo:大便
```