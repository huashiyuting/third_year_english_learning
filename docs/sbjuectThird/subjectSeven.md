<!--
 * @Author: hua
 * @Date: 2020-02-08 09:04:21
 * @description: 
 * @LastEditors: hua
 * @LastEditTime: 2020-08-18 11:29:06
 -->
```
sneezing:打喷嚏
ranging：周围
pin：掐
merely:仅仅
worldwide:世界范围
stand on:站在
authorities:权威
speed:速度
drug:药物
surgery:外科手术
curiously:好奇心
clue:线索
ancient:古老的
superstition:信仰
amazing:adj. 令人惊异的
bodily:adv. 全身地，以整个身体地；整个地，完全地；亲自地
reaction:反应
mind:感觉
aristotle:腕关节
heartily：adv. 衷心地；热忱地；痛快地
interpretation：解释
belief:相信
scholastic:adj. 学校的；学者的；学术的（等于scholastical）
vocational:职业
adjustment:调整
reassurance：保证
unreasonable:不合理
fear:害怕
tuberculosis:n. 肺结核；结核病
report:报告
subject：主题
wonder:v. 想知道；（用于询问时）不知道；感到疑惑；感到诧异；感到惊叹
awe:惊叹
puzzlement:迷惘
begging:恳求
universal:通用
indication:表明
beg:vt.& vi. 乞讨;恳求（原谅）;回避（问题）;
bless:vt. 祝福;保佑;赞美;为…祈福;
practice:实践
trace:追溯
ancient:古代
custom:风俗
equivalent:相等
rid of:摆脱
annoying:烦人
mechanism：n. [生]机制，机能，[乐]机理;（机械）结构，机械装置[作用]，（故事的）结构;[艺]手法，技巧，途径;机械作用;
evil:恶
spirit:精神
strangely:异常的
possessed:拥有
remarkable:adj. 异常的，引人注目的，;卓越的;显著的;非凡的，非常（好）的;
conscious:意识到
continuously:连续不断
mentally:精神上德
mental：精神
burden:负担
treatment:治疗
ancient:古代
superstition:迷信
automobile:汽车
worst：最坏
literally:字面意思
worldwide:全球
occasions:某次
decade：n. 十年，十年间;十个一组;十年期;
climate:气候
entire:全
concentration:集中
manufacturing:成批制造
reflect：反映
consider:仔细考虑
carbon dioxide:二氧化碳
temperature:温度
polar：极地
ice cap:冰盖
melt:融化
widely:adv. 广泛地;普遍地;到处;大量地;
particular:专指
atmosphere:大气
equally:平等
disastrous:灾难
agriculture:农业
expert:专家
drafted:卸任
conclude：断定
greenhouse:温室效应
tendencies:趋势
driven：驱动
economic：经济
profit:利润
neglect:忽略
damage:损害
worthwhile:值得
civilization：先进文明
countryside:乡村
widespread:分布广
damage:损坏
urban:城市
industries:工业
occurrence:发生的事情
ungrounded:无根据
reject:拒绝
slip into:竞赛
make up for：弥补
set up:创建
concluded:被断定
degree:度
lowering:下降
disaster:灾难
remain:保持
constant:不断
variation:变化
primarily:主要
burning:强烈
fossil:顽固
fuels:争论
likelihood:可能性
economic:经济
otherwise：否则
some day:有一天
give for:牺牲
achieve:vt. 取得;获得;实现;成功;
sake:理由
raise:抚养
resume:概要
step:阶段
kindergarten:幼儿园
lend:借
inferior:adj. （质量等）低劣的;下级的，下等的;（位置在）下面的;[植]下位的;
particularly:尤其
maternal:adj. 母亲的;母亲般的;母系的;母亲方面的
pursuit:追求
energy:精力
instincts:本能
assistant:助手
naturally:自然地
reproduction:繁殖
meant:为...而做
material:原料
financial:财务
permit:许可
situation：状态
equal:相等
expect:预期
sacrifice:牺牲
balance:平衡
paternal:父亲
duty:负责
weekly:每周
apart:分开
national:自然地
circulation：循环
rely:依赖
prefer:喜欢
council:地方区会议
district:地区
regularly：有规律的
supplied:有...提供
opportunity:机会
grateful:感激
comment:一轮
political:adj. 政治的;政党的;对政治有兴趣的;
persuade:劝说
community:社会
service：服务
copy:副本
press:报刊
surprise：惊讶
circulation：循环
anxious:焦虑
cell phone:手机
radiation:辐射
studio:n. 工作室，画室;[影]演播室，电影制片厂;
headaches:头疼
disorder:失调
concert:协调
evidence:证据
whether:无论
trial:困难
risk:危险
bias:偏见
rate: n. 速度;比率;等级;（利息等的）费率;
consumption:消耗
scan:扫描
cell：细胞
inquiry:查询
conclusive:确凿
derive:产生
mission：使命
regions:n. 地区( region的名词复数 );[数学]区域;（艺术、科学等的）领域;行政区;
switched：转换
particular:专指
immensely:极端的
contribute:促成
participate：参与
provide:提供
surprisingly:令人惊讶的
```