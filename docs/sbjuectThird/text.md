<!--
 * @Author: hua
 * @Date: 2020-03-20 10:03:52
 * @description: 
 * @LastEditors: hua
 * @LastEditTime: 2020-03-20 12:02:16
 -->

### penetrate
```
v.穿透，渗透
The intense radiation of the unclear leak can penetrate the skin.
核泄漏的高强度辐射会穿透皮肤。
```
### surrender
```
n.touxiang,v.touxiang
The soldier who is loyal to his country refuse to surrender himself to the enemy.
zhexieshibingzhongyuzuguo,jujuexiangdirentouxiang.
```
### pitch
```
n.liqing,yingao,xuanchuan,yundong
A basic sense of rhythm and pitch is essential in a music teacher.
jibendexuanlvganhegaoyingganshiyigyinyuelaoshidebibeiyaosu
```
### priest
```
n.shenfu,shenzhirenyuan
He has been a christian priest in a German church since 1996.
talaizi1996nianjiuyizhizaideguoyizuojidujiaotangdanrenmushi.
```
### rib
```
n.paigu,leigu,tuzhuanghuawen
He fell off a ladder and broke his ribs.
tacongtizishangshuaixiashangletadeleigu.
```
### mutual
```
n.xianghude,bicide
```
### seal
```
v.mifeng
Make sure you`ve signed the cheque before sealing the envelops.
yidingyaozaizhipiaoshangqianlemingzaimifeng
```
### dimension
```
n.rongji
computer design tools that work in three dimension.
jisuanjisanweishejigongju.
```
### haul
```
v.tuodong n.yipi 
She hauled the box into the room.
taruozhehezijinrulefangjian.
```
### keen
```
adj.rezhongde,renqingde n.wange
She is always keen to help.
tazongshileyuzhuren.
```
### manipulate
```
v.shuliangdecaozuo,caokong
She manipulated the lights to get just the effect she wanted.
tashuliandecaozuodengguang,yidadaotashuoyaodexiaoguo 
```
### whale
```
n.jing
```
### corridor
```
n.zoudao
```
### urgent
```
adj.jinpode
an urgent appeal(kenqiu) for information.
jinjihuyutigongxinxi
```
### forth
```
adv.xiangwai,xiangqian
He was very upset(沮丧) and paced back and forth.
tashifendejusang,laihuiduobu
```
### external
```
She external appearance of the building.
ta kan le zhe jian zhu de wai guan 
```
### aggressive
```
adj.haodong,jingzhengxin
```
### odd
```
adj.dangede,shengyude,guguai
The painter's works are an odd mixture(hunhe) of difference styles.
zheweihuajiadezuopishibutongfenggedeqiguaihunhe
```
### doubtful
```
adj.nabuzhunde
He shook his head doubtfully
tahuaiyideyaodongtou.
```
### prediction
```
n.yuyan,yugao
The results of the experiment confirmed our predictions.
shiyanjieguozhengminglewomndeyuce.
```
### probable
```
adj.bianxiede
```
### upright
```
adj.chuzhi
```
### backward
```
adj.xianghou
She strode past him without a backward glance
```
### skilled
```
adj.youjiqiaode
This is highly-skilled work that requires long experience.
zhefengaojiqiaodegongzuoxuyaohenchangdejingyan
```
### formation
```
n.xingcheng,zucheng,jiegou
My parents had an important influence on the formation of my personality.
wodejiarenyouyigfeichangzhongyaodeyinglichanshengduiyuwodexingge
```
### eliminate
```
v.xiaochu
Three suspects have been eliminated from this investigation.
sangegexianyifanbeixiaochuhuaiyizaizhecidiaochazhong.
```
### mystery
```
n.shenmidedeshiwu
```
### migrate
```
v.yidong,qiaoyi
```
### comprehensive
```
adj.quanmiande
they did a comprehensive study on learning styles.
tamendexuexifenggeshiyixiangquanmiandeyyanjiu.
```
### abundance
```
n.daliang
There was an abundance of corn last year.
qunianyumifengshou.
```
### offend
```
v.maofan
The unkind man always says mean words to offend others.
zhegerenhenbuyoushan.zongshuoxiekebodehuamaofanta
```
### canal
```
n.gouqu
```
### heir
```
v.jicheng
```
### fleet
```
n.jiandui,chedui
```
### rub
```
vt.yongshouca
Scolded by his mother.this poor boy cried and rubbed this eyes.
aileyidunmamadema,zhegekeliandehaiziyongshoucazheyanjingku.
```
### shrug
```
n.songjian
The man shrugged his shoulders and left without another word.
nananzishouleshoujianranhouyiyanbufdezoule.
```
### bubble
```
n.泡沫      
```
### chip
```
n.芯片
The entire content of a book will be located on a single silicon ship.
zhengbenshudeneirongzhiyongyipianguixinpianjiukeyizhuangxia
```
### fragment
```
n.碎片
Police found fragment of glass on the floor.
jingchazaidibanzhaofaxianlebolisuipian.
```
