<!--
 * @Author: hua
 * @Date: 2020-02-07 20:40:28
 * @description: 
 * @LastEditors  : hua
 * @LastEditTime : 2020-02-08 08:57:52
 -->
```
elastic:n. 松紧带；橡皮圈
adj. 有弹性的；灵活的；易伸缩的
British:英国
society:社会
sense:感觉
European:欧洲
narrow:狭窄
economic:经济
thus:从而
unit:单元
importance:重要
tremendous:远大
hence：因此
peculiarly:特别
independent:独立
entirely:完全
responsible：责任
rid of:摆脱
financially:经济
pair:一对男女
aunt:叔叔
uncle：阿姨
except：例外
dowry:嫁妆
side:面
define:定义
provide:提供
decide:清楚地
household：家喻户晓
blindness:失明
disabling:残废
totally:彻底
sight:视力
became:赞成
composer:作曲家
pianist:刚请假
wide:广泛
outstanding contributions:勋绩
dependent:依赖
bang:碰撞
beat:拍击
drums:鼓
concentrate:专注
imitate：模仿
entertainment:娱乐
instrument:仪器
piano:钢琴
junior：年幼
church： n. 教堂；礼拜；教派
rhythm:节奏
tunes:曲调
session:一段
immediate：立即
recognition：识别
president:校长，总统
record:记录
smash:流行
wonder:奇人
musician：音乐家
blind:失明
entitle:称作
mentions:提及
performance:表现
instant fame:瞬间成名
fingertips:指尖
determine:决心
present:目前
impose：不可能
force:影响
beyond：超出
predict：预言
unbelievable:不相信
strength of:凭借
performance:业绩
electrical:电的
perform:vt. 执行；完成；演奏
vi. 执行，机器运转；表演
properly:适当的
fascinating:迷人的
chemical：化学品
desire：渴望
fat:脂肪，变胖
stimulate:刺激
overeat:吃过量
academic:美术
appetite:胃口
dramatically：戏剧的
certain:确定
condition：条件
alternative:可替换的
affect:影响
traditional:传统
notion:观念
pill:n. 药丸；弹丸，子弹；口服避孕药
vt. 把…制成丸剂；使服用药丸；抢劫，掠夺（古语）
vi. 做成药丸；服药丸
tension： n. 张力，拉力；紧张，不安；电压
overweight:太胖
focus:集中
threat:威胁
prescribed:adj. 规定的
western:n. 西方人；西部片，西部小说
diet:日常饮食
exercise:运动
fit: n. 合身；发作；痉挛
adj. 健康的；合适的；恰当的；准备好的
vt. 安装；使……适应；使……合身；与……相符
drug:药品
brand:品牌
advertising:广告
executive：行政部门
platform：平台
pick:挑
purchase:购买
severe:严格
hundreds of millions of：成千上万
standard:标准
feature:n. 特色，特征；容貌；特写或专题节目 
firm:n. 商行；商号；公司
adj. 坚定的；牢固的；严格的；结实的；坚决的；牢牢控制的；坚挺的
adv. 坚信；坚持；稳固地
theorized: vi. 建立理论或学说；推理
vt. 建立理论
originally: adv. 最初，起初；本来
debate:n. 辩论；（正式的）讨论
v. （尤指正式）讨论，辩论；仔细考虑
opted:选择
potentially:adv. 可能地，潜在地
ceiling:n. 天花板；上限
afford:负担
flock:n. 群；棉束（等于floc）
vi. 聚集；成群而行
vt. 用棉束填满
simplicity:简单
expect：不包括
offer:提供
race:n. 属，种；种族，人种；家庭，门第；赛事，赛跑
raging:强烈
chief:首领
officer:n. 军官，警官；公务员，政府官员；船长
executive:经理
existence:存在
househusband:家庭煮夫
retired:退休
arrangement:排列
fortune:财富
point:指
senior:级
trend:趋势
dramatic:adj. 戏剧的；急剧的；引人注目的；激动人心的
among:在。。之中
unemployed:失业
wage:工资
earners:阶段
appealing:吸引人
conflict:冲突
elementary:基本
female:女
occupation:工作
position:位置
recession:经济
unusual：不寻常
currently:adv. 当前；一般地
ensure:承诺
fill:充满
occurring:发生
regularly:有规律的
```