module.exports = {
    title: '聊天室文档',  // 设置网站标题
    description : '作者很懒，所以这里不知道写什么',
    base : '',
    themeConfig : {
      nav : [
          /* { text: '接口定义', link: '/apiword' },
          { text: '接口字段定义', link: '/api' },
          { text: '附录：错误码', link: '/error' } */
      ],
      sidebar: {
          '/' : [/* {
            title: '布局类组件',
            collapsable: true,
            children: [
                ["/",'介绍'], //指的是根目录的md文件 也就是 README.md 里面的内容
                ["apiword", '消息界面'],
                ["api", '消息界面'],
                ["error", '消息界面']
            ]}, */
            ["/",'介绍'], //指的是根目录的md文件 也就是 README.md 里面的内容
            {
              title: '客户端',
              collapsable: true,
              children: [
                ["client/home", '消息界面'],
                ["client/addressBook", '通讯录界面'],
                ["client/search", '搜索界面'],
                ["client/person", '个人界面']]
            },
            ["about", '关于我们']
          ]
      },
      sidebarDepth : 2
    }
  }